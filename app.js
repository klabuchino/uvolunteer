var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var mongoose = require('mongoose');
var session = require('express-session');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var config = require('./config');
var BaseUser = require('./models/users/BaseUser.js').BaseUser;
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var projectRouter = require('./routes/project');

mongoose.set('useUnifiedTopology', true);
var mConnection = mongoose.connect(config.mongoUrl, {
    useNewUrlParser: true
});


var MongoStore = require('connect-mongo')(session);

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

mongoose.set('useCreateIndex', true);


config.session.store = new MongoStore({mongooseConnection: mongoose.connection});
app.use(session(config.session));
app.use(passport.initialize());
app.use(passport.session());

passport.use(new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password'
},
  BaseUser.authorize.bind(BaseUser))
);
passport.serializeUser(function(user, done) {
  console.log('serializeUser', user);
  done(null, user._id);
});

passport.deserializeUser(function(id, done) {
  console.log('deserializeUser', id);
  BaseUser.findById(id, function(err,user){
    err
      ? done(err)
      : done(null,user);
  });
});

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/project', projectRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
});

module.exports = app;
