import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import OrganizerPage from '../pages/organizer/organizer-page';
import ProjectListPage from '../pages/project/project-list-page'
import ProjectPage from '../pages/project/project-page';
import Header from '../components/Header';
import Footer from '../components/Footer';
import SideNav from '../components/SideNav';
import UpdateProjectModal from '../modals/update-project-modal';

class OrganizerSwitcher extends Component {
    constructor() {
        super();
        this.menuItems = [
            {title: "Добавить", icon: "add_circle", data: [
                {title: "Публикация", icon: "bookmark"},
                {title: "Проект", icon: "dehaze", onClick: this.openUpdateProjectModal.bind(this)}
            ]},
            {title: "Избранные", icon: "bookmark", data: []},
            {title: "Мои публикации", icon: "dehaze", data: []}
        ]
    }

    openUpdateProjectModal(proj, e) {
        this.updateProjectModal.showModal(proj, e);
    }

    render() {
        return (
            <>
                <UpdateProjectModal updateProject={this.props.updateProject} ref={(node) => {this.updateProjectModal = node}} />
                <Header {...this.props} headerLinkTo="/" />
                <SideNav data={this.menuItems} />
                <Route exact path={`${this.props.match.path}`} component={OrganizerPage} />
                <Route exact path={`/projects`} component={ProjectListPage} />
                <Route exact path={`/projects/:id_project`} component={ProjectPage} />
                <Footer />
            </>
        );
    }
}

export default OrganizerSwitcher;
