import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import VolounteerPage from '../pages/volounteer/volounteer-page';
import Header from '../components/Header';
import Footer from '../components/Footer';

class GuestSwitcher extends Component {
    constructor() {
        super();
        
    }

    render() {
        return (
            <>
                <Header {...this.props} headerLinkTo="/" />
                <Route exact path={`${this.props.match.path}`} component={VolounteerPage} />
                <Footer />
            </>
        );
    }
}

export default GuestSwitcher;
