import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import VolounteerPage from '../pages/volounteer/volounteer-page';
import Header from '../components/Header';
import Footer from '../components/Footer';
import SideNav from '../components/SideNav';

class VolounteerSwitcher extends Component {
    constructor() {
        super();
        this.menuItems = [
            {title: "Избранные", icon: "bookmark", data: []}
        ]
    }

    render() {
        return (
            <>
                <Header {...this.props} headerLinkTo="/" />
                <SideNav data={this.menuItems} />
                <Route exact path={`${this.props.match.path}`} component={VolounteerPage} />
                <Footer />
            </>
        );
    }
}

export default VolounteerSwitcher;
