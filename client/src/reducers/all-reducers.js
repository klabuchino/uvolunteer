import {combineReducers} from 'redux';
import {userReducer} from './user-reducers';
import {projectReducer} from './project-reducers';

const allReducers = combineReducers({
    user: userReducer,
    project: projectReducer
});

export default allReducers;
