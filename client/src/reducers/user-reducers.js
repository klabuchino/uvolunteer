import {types} from "../redux-types";
import M from 'materialize-css';


const initialState = {
  me: {},
  loading: false
}

export const userReducer = (state = initialState, action) => {
    const {type, payload} = action

    switch (type) {


        case types.REGISTER: {
            return {
                ...state,
                loading: true,
            }
        }

        case types.REGISTER_SUCCESS: {
            M.toast({html: `Регистрация прошла успешно!`});
            return {
                ...state,
                loading: false
            }
        }

        case types.REGISTER_ERROR: {
            M.toast({html: `Ошибка регистрации: ${payload}`});
            return {
                ...state,
                error: payload,
                loading: false
            }
        }


        case types.AUTH: {
            return {
                ...state,
                loading: true
            }
        }

        case types.AUTH_SUCCESS: {
            return {
                ...state,
                me: payload,
                loading: false
            }
        }

        case types.AUTH_ERROR: {
            M.toast({html: `Ошибка авторизации: ${payload}`});
            return {
                ...state,
                error: payload,
                loading: false
            }
        }

        case types.LOGOUT: {
            return {
                ...state,
                me: {
                    role: "none"
                }
            }
        }

        case types.INIT: {
            return {
                ...state,
                loading: true
            }
        }

        case types.INIT_SUCCESS: {
            return {
                ...state,
                me: payload,
                loading: false
            }
        }

        case types.INIT_ERROR: {
            return {
                ...state,
                error: payload,
                loading: false
            }
        }

        default: {
            return {...state}
        }
    }
}
