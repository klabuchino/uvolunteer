import {types} from "../redux-types";
import M from 'materialize-css';


const initialState = {
  list: [],
  current: {},
  loading: false
}

export const projectReducer = (state = initialState, action) => {
    const {type, payload} = action

    switch (type) {
        case types.GET_ONE_PROJECT: {
            return {
                ...state,
                loading: true,
            }
        }

        case types.GET_ONE_PROJECT_SUCCESS: {
            return {
                ...state,
                current: payload,
                loading: false
            }
        }

        case types.GET_ONE_PROJECT_ERROR: {
            return {
                ...state,
                error: payload,
                loading: false
            }
        }


        case types.GET_PROJECT: {
            return {
                ...state,
                loading: true,
            }
        }

        case types.GET_PROJECT_SUCCESS: {
            return {
                ...state,
                list: payload,
                loading: false
            }
        }

        case types.GET_PROJECT_ERROR: {
            return {
                ...state,
                error: payload,
                loading: false
            }
        }


        case types.UPDATE_PROJECT: {
            return {
                ...state,
                loading: true,
            }
        }

        case types.UPDATE_PROJECT_SUCCESS: {
            M.toast({html: `Проект создан/обновлен успешно!`});
            return {
                ...state,
                list: payload,
                loading: false
            }
        }

        case types.UPDATE_PROJECT_ERROR: {
            M.toast({html: `Ошибка создания/обновления: ${payload}`});
            return {
                ...state,
                error: payload,
                loading: false
            }
        }

        default: {
            return {...state}
        }
    }
}
