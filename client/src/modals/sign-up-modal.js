import React, { Component } from 'react';
import {Modal} from 'react-materialize';
import { log } from 'util';

class SignUpModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            current_role: 0,
            fullName: "",
            age: "",
            socialNetwork: "",
            password: "",
            confirm_password: "",
            phoneNumber: "",
            email: "",
            passportNummber: "",
            step: 1,
            roles: [
                {title: "Волонтёр", step: 1, total_steps: 2},
                {title: "Организатор"}
            ]
        }
    }

    onChange(e) {
        this.setState({[e.target.name]: e.target.value})
    }

    _renderInputs(inputs) {
        return inputs.map((el, ind) => {
            return (<div className="input-field" key={ind}><input 
                onChange={this.onChange.bind(this)} 
                value={this.state[el.name]} 
                name={el.name} 
                id={el.name} 
                type={el.type} 
            /><label htmlFor={el.name}>{el.placeholder}</label></div>);
        })
    }

    _renderFirstStepVol () {
        let inputs = this._renderInputs([
            {placeholder: "ФИО", name: "fullName", type: "text"},
            {placeholder: "Почта", name: "email", type: "text"},
            {placeholder: "Возраст", name: "age", type: "text"},
            {placeholder: "Ссылка на соц. сети", name: "socialNetwork", type: "text"}
        ]);
        return (
            <>
            {inputs}
            <button className="bck_btn" onClick={() => {this.setState({step: 2})}}>Продолжить</button>
            </>
        )
    }

    register() {
        this.props.register({
            ...this.state,
            role: this.state.current_role === 0 ? 'volunteer': 'organizer'
        });
        this.setState({
            current_role: 0,
            fullName: "",
            age: "",
            socialNetwork: "",
            password: "",
            confirm_password: "",
            phoneNumber: "",
            email: "",
            passportNummber: "",
            step: 1,
        }, this.modal.hideModal());
    }

    _renderSecondStepVol () {
        let inputs = this._renderInputs([
            {placeholder: "Введите пароль", name: "password", type: "password"},
            {placeholder: "Подтвердите пароль", name: "confirm_password", type: "password"},
            {placeholder: "Номер телефона", name: "phoneNumber", type: "text"}
        ]);
        return (
            <>
            {inputs}
            <div className="actions">
                <button className="border_btn" onClick={() => {this.setState({step: 1})}}>Назад</button>
                <button className="bck_btn" onClick={this.register.bind(this)}>Зарегистрироваться</button>
            </div>
            
            </>
        )
    }

    _renderOrganizerForm () {
        let inputs = this._renderInputs([
            {placeholder: "ФИО", name: "fullName", type: "text"},
            {placeholder: "Почта", name: "email", type: "text"},
            {placeholder: "Введите пароль", name: "password", type: "password"},
            {placeholder: "Юридические данные", name: "passportNummber", type: "text"},
        ]);
        return (
            <>
            {inputs}
            <button className="bck_btn" onClick={this.register.bind(this)}>Зарегистрироваться</button>
            
            <p style={{"fontSize": "13px", "marginTop": "18px", "marginBottom": "0"}}><span>Зарегистрироваться через Госуслуги</span></p>
            </>
        )
    }

    getModalRef(node) {
        this.modal = node;
    }

    showModal(e) {
        this.setState(this.modal.showModal(e));
    }

    selectRole(ind) {
        this.setState({current_role: ind});
    }

    render() {
        var steps = this.state.roles.map((el,ind) => {
            let display_steps = ind === this.state.current_role && el.total_steps ? 
                                        (<span>(Шаг {this.state.step} из {el.total_steps})</span>) :
                                        "";
            return (
                <div 
                    className={ind === this.state.current_role ? "active" : ""} 
                    key={ind} 
                    onClick={() => {this.selectRole.bind(this)(ind)}}>
                    <div>
                        <span>{el.title}</span>
                        {display_steps}
                    </div>
                </div>
            )
        });

        var content;
        if (this.state.current_role === 0) {
            switch(this.state.step) {
                case 1:
                    content = this._renderFirstStepVol.bind(this)();
                    break;
                case 2:
                    content = this._renderSecondStepVol.bind(this)();
                    break;
                default:
                    break;
            }
            
        } else {
            content = this._renderOrganizerForm.bind(this)();
        }

        return (
          <Modal
            ref={this.getModalRef.bind(this)}
            className="sign_modal">
            <>
            <div className="modal__header">
                {steps}
            </div>
            <div className="modal__content">
                {content}
            </div>
            </>
          </Modal>
        );

    }
}

export default SignUpModal;