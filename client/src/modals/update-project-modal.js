import React, { Component } from 'react';
import {Modal} from 'react-materialize';

class UpdateProjectModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: "",
            description: "",
            image: {},
            tags: [],
            resources: [], // title, cost, maxQuantity
            current_resource_id: 0,
            current_resource_title: "",
            current_resource_cost: "",
            current_resource_maxQuantity:"",
            volunteers: [],
            current_volunteer_id: 0,
            current_volunteer_title: "",
            current_volunteer_description: "",
            current_volunteer_maxQuantity: ""
        }
        this.allTags = [{title: "Экология", _id: 0}, {title: "Урбанизм", _id: 1}, {title: "Стрит-арт", _id: 2}]
    }

    onChange(e) {
        this.setState({[e.target.name]: e.target.value})
    }

    getModalRef(node) {
        this.modal = node;
    }

    showModal(project, e) {
        this.setState({...project}, this.modal.showModal(e));
    }

    selectTag(tag) {
        let isExists = this.state.tags.filter(el => el._id === tag._id).length > 0;

        if (isExists) {
            this.setState({tags: this.state.tags.filter(el => el._id !== tag._id)})
        } else {
            this.setState({tags: this.state.tags.concat(tag)});
        }
    }

    _renderResources() {
        return this.state.resources.map((res, ind) => {
            return (
                <div className="resources__item" key={ind}>
                    <span>{res.title}</span>
                    <span>{res.cost}₽</span>
                    <span>{res.maxQuantity}шт.</span>
                    <span><i onClick={this.removeResource.bind(this, res)} className="material-icons">close</i></span>
                </div>
            )
        })
    }

    _renderVolunteers() {
        return this.state.volunteers.map((vol, ind) => {
            return (
                <div className="volunteers__item" key={ind}>
                    <span>{vol.title}</span>
                    <span>{vol.description}</span>
                    <span>{vol.maxQuantity}чел.</span>
                    <span><i onClick={this.removeVolunteer.bind(this, vol)} className="material-icons">close</i></span>
                </div>
            )
        })
    }

    addResource() {
        this.setState({resources: this.state.resources.concat({
            _id: this.state.current_resource_id,
            title: this.state.current_resource_title,
            cost: this.state.current_resource_cost,
            maxQuantity: this.state.current_resource_maxQuantity
        })}, () => {this.setState({
            current_resource_id: this.state.current_resource_id + 1,
            current_resource_title: "",
            current_resource_cost: "",
            current_resource_maxQuantity:""
        })});
    }

    removeResource(res) {
        this.setState({
            resources: this.state.resources.filter(el => el._id !== res._id)
        }, () => {this.setState({current_resource_id: this.state.current_resource_id + 1})})
    }

    addVolunteer() {
        this.setState({volunteers: this.state.volunteers.concat({
            _id: this.state.current_volunteer_id,
            title: this.state.current_volunteer_title,
            description: this.state.current_volunteer_description,
            maxQuantity: this.state.current_volunteer_maxQuantity
        })}, () => {this.setState({
            current_volunteer_id: this.state.current_volunteer_id + 1,
            current_volunteer_title: "",
            current_volunteer_description: "",
            current_volunteer_maxQuantity:""
        })});
    }

    removeVolunteer(vol) {
        this.setState({
            volunteers: this.state.volunteers.filter(el => el._id !== vol._id)
        }, () => {this.setState({current_volunteer_id: this.state.current_volunteer_id + 1})})
    }

    updateProject() {
        this.props.updateProject({
            ...this.state,
            resourcesArray: JSON.stringify(this.state.resources),
            volunteersItemsArray: JSON.stringify(this.state.volunteers),
            tags: JSON.stringify(this.state.tags),
            image: ""
        });
        this.setState({
            title: "",
            description: "",
            image: {},
            tags: [],
            resources: [], // title, cost, maxQuantity
            current_resource_id: 0,
            current_resource_title: "",
            current_resource_cost: "",
            current_resource_maxQuantity:"",
            volunteers: [],
            current_volunteer_id: 0,
            current_volunteer_title: "",
            current_volunteer_description: "",
            current_volunteer_maxQuantity: ""
        }, () => {this.modal.hideModal()})
    }

    render() {
        return (
          <Modal
            ref={this.getModalRef.bind(this)}
            className="update_project_modal">
            <>
            <p className="title">Информация о проекте</p>
            <div className="main_fields">
                <div className="title_and_tags">
                    <div className="input-field">
                        <input 
                            onChange={this.onChange.bind(this)} 
                            value={this.state.title} 
                            name="title"
                            id="title"
                            type="text"
                        />
                        <label htmlFor="title">Название проекта</label>
                    </div>
                    <div className="tags">
                        {this.allTags.map((el,ind) => {
                            let isExists = this.state.tags.filter(tag => el._id === tag._id).length > 0;
                            return (
                                <span 
                                    key={ind} 
                                    className={isExists ? "active" : ""}
                                    id={el._id}
                                    onClick={this.selectTag.bind(this, el)}
                                >
                                    {el.title}
                                </span>
                            )
                        })}
                    </div>
                </div>
                <div className="file">
                    <span><i className="material-icons">attach_file</i><span>Добавить изобр.</span></span>
                    <input type="file" />
                </div>
            </div>
            <div className="input-field">
                <textarea onChange={this.onChange.bind(this)} value={this.state.value} name="description" id="textarea1" className="materialize-textarea"></textarea>
                <label htmlFor="textarea1">Краткое описание проекта</label>
            </div>
            <p className="title">Ресурсы и бюджет</p>
            <div className="recources">
                {this._renderResources.bind(this)()}
                <div className="resources__form">
                    <div className="input-field">
                        <input 
                            onChange={this.onChange.bind(this)} 
                            value={this.state.current_resource_title} 
                            name="current_resource_title"
                            id="current_resource_title"
                            type="text"
                        />
                        <label htmlFor="current_resource_title">Название ресурса</label>
                    </div>
                    <div className="input-field">
                        <input 
                            onChange={this.onChange.bind(this)} 
                            value={this.state.current_resource_cost} 
                            name="current_resource_cost"
                            id="current_resource_cost"
                            type="text"
                        />
                        <label htmlFor="current_resource_cost">Цена, ₽</label>
                    </div>
                    <div className="input-field">
                        <input 
                            onChange={this.onChange.bind(this)} 
                            value={this.state.current_resource_maxQuantity} 
                            name="current_resource_maxQuantity"
                            id="current_resource_maxQuantity"
                            type="text"
                        />
                        <label htmlFor="current_resource_maxQuantity">Кол-во</label>
                    </div>
                    <div className="input-field">
                        <div className="adding_btn" onClick={this.addResource.bind(this)}>
                            <span>Добавить</span>
                            <i className="material-icons">add_circle</i>
                        </div>
                    </div>
                </div>
            </div>
            <p className="title">Волонтёры</p>
            <div className="volunteers">
                {this._renderVolunteers.bind(this)()}
                <div className="volunteers__form">
                    <div className="input-field">
                        <input 
                            onChange={this.onChange.bind(this)} 
                            value={this.state.current_volunteer_title} 
                            name="current_volunteer_title"
                            id="current_volunteer_title"
                            type="text"
                        />
                        <label htmlFor="current_volunteer_title">Что за волонтёр?</label>
                    </div>
                    <div className="input-field">
                        <input 
                            onChange={this.onChange.bind(this)} 
                            value={this.state.current_volunteer_description} 
                            name="current_volunteer_description"
                            id="current_volunteer_description"
                            type="text"
                        />
                        <label htmlFor="current_volunteer_description">Что делает?</label>
                    </div>
                    <div className="input-field">
                        <input 
                            onChange={this.onChange.bind(this)} 
                            value={this.state.current_volunteer_maxQuantity} 
                            name="current_volunteer_maxQuantity"
                            id="current_volunteer_maxQuantity"
                            type="text"
                        />
                        <label htmlFor="current_volunteer_maxQuantity">Кол-во</label>
                    </div>
                    <div className="input-field">
                        <div className="adding_btn" onClick={this.addVolunteer.bind(this)}>
                            <span>Добавить</span>
                            <i className="material-icons">add_circle</i>
                        </div>
                    </div>
                </div>
            </div>
            <button onClick={this.updateProject.bind(this)} className="border_btn">Создать проект</button>
            </>
          </Modal>
        );

    }
}

export default UpdateProjectModal;