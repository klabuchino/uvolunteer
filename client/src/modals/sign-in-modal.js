import React, { Component } from 'react';
import {Row, Modal, Tabs, Tab, Button, Col} from 'react-materialize';

class SignInModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: ""
        }
    }
    getModalRef(node) {
        this.modal = node;
    }

    showModal(e) {
        this.setState(this.modal.showModal(e));
    }

    onChange(e) {
        this.setState({[e.target.name]: e.target.value})
    }

    _renderInputs(inputs) {
        return inputs.map((el, ind) => {
            return (<div className="input-field" key={ind}><input 
                onChange={this.onChange.bind(this)} 
                value={this.state[el.name]} 
                name={el.name} 
                id={el.name} 
                type={el.type} 
            /><label htmlFor={el.name}>{el.placeholder}</label></div>);
        })
    }

    auth() {
        this.props.auth(this.state.email, this.state.password);
        this.setState({email: "", password: ""}, this.modal.hideModal());
    }


    render() {
        let inputs = this._renderInputs([
            {placeholder: "Почта", name: "email", type: "text"},
            {placeholder: "Пароль", name: "password", type: "password"},
        ]);
        return (
          <Modal
            ref={this.getModalRef.bind(this)}
            className="sign_modal">
            
            <div className="modal__content">
                <p>Вход</p>
                <p style={{"fontSize": "13px"}}>Ещё нет аккаунта? <span>Создайте его сейчас!</span></p>
                {inputs}
                <div className="actions">
                    <button className="clear_btn">Забыли пароль?</button>
                    <button onClick={this.auth.bind(this)} className="bck_btn" style={{"marginRight": "30px"}}>Войти</button>
                </div>
                <p style={{"fontSize": "13px", "marginTop": "18px", "marginBottom": "0"}}><span>Войти с помощью Госуслуги</span></p>
            </div>

          </Modal>
        );

    }
}

export default SignInModal;