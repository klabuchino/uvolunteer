import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Preloader, Row, Col} from 'react-materialize';
import { actionCreators } from './actions/all-actions.js';
import VolounteerSwitcher from './switchers/volounteer-switcher';
import GuestSwitcher from './switchers/guest-switcher';
import OrganizerSwitcher from './switchers/organizer-switcher';
import $ from 'jquery';
import './App.css';

class App extends Component {
    componentDidMount() {
        this.props.init(this.props.history);
    }

    addClassForBody(role) {
        if(role) $("body").addClass(`${role}_page`);
    }

    render() {
        let {role} = this.props.user.me;
        console.log(role)
        if (role && !this.props.loading) {
            this.addClassForBody(role);
            switch (role) {
                case "volunteer":
                    return (
                        <VolounteerSwitcher {...this.props} />
                    )

                case "organizer":
                    return (
                        <OrganizerSwitcher {...this.props} />
                    )

                default:
                    return (
                        <GuestSwitcher {...this.props} />
                    )
            }
        }
        return (
            <Row style={{display: "flex", justifyContent: "center", alignItems: "center", height: "100vh"}}>
              <Col>
                <Preloader size='big'/>
              </Col>
            </Row>
        );
    }
}

const mapStateToProps = (state) => ({
    user: state.user
});

const mapDispatchToProps = (dispatch) => {
    return {
        updateProject: (props) => {
            return dispatch(actionCreators.updateProject(props)).payload.then((res) => {
                !res.error ? dispatch(actionCreators.updateProjectSuccess(res.data)) :
                dispatch(actionCreators.updateProjectError(res.error));
            });
        },
        auth: (email, password) => {
            return dispatch(actionCreators.auth(email, password)).payload.then((res) => {
                !res.error ? dispatch(actionCreators.authSuccess(res.data.user)) :
                dispatch(actionCreators.authError(res.error));
            });
        },
        register: (user) => {
            return dispatch(actionCreators.register(user)).payload.then((res) => {
                !res.error ? dispatch(actionCreators.registerSuccess(res)) :
                dispatch(actionCreators.registerError(res.error));
            });
        },
        init: (history) => {
            return dispatch(actionCreators.init(history)).payload.then((res) => {
                !res.error ? dispatch(actionCreators.initSuccess(res, history)) :
                dispatch(actionCreators.initError(res.error));
            });
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
