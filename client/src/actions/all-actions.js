import {getRequest, postRequest} from "../helpers.js";
import {types} from "../redux-types.js";
import {userActions} from "./user-actions.js";
import {projectActions} from "./project-actions.js";


// Helper functions to dispatch actions, optionally with payloads
export const actionCreators = {
    ...userActions,
    ...projectActions,


    init: () => {
        let data = getRequest("/users/init", {});
        return { type: types.INIT, payload: data }
    },
    initSuccess: (data, history) => {
        console.log(data)
        if (!data.role) {}
        return { type: types.INIT_SUCCESS, payload: data }
    },
    initError: (error) => {
        console.log('init err: ', error);
        return { type: types.INIT_ERROR, payload: error }
    },    


    auth: (email, password) => {
        let req = postRequest( "/users/login", { email, password });
        return { type: types.AUTH, payload: req }
    },
    authSuccess: (me) => {
        console.log('auth data: ', me);
        return { type: types.AUTH_SUCCESS, payload: me }
    },
    authError: (error) => {
        console.log('auth err: ', error);
        return { type: types.AUTH_ERROR, payload: error }
    },
    logout: () => {
        let req = postRequest("/users/logout", {});
        return { type: types.LOGOUT, payload: req }
    }
}
