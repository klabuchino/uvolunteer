import {getRequest, postRequest} from "../helpers.js";
import {types} from "../redux-types.js";


export const projectActions = {

    getProjects: () => {
        let data = getRequest("/project/getAll", {});
        return { type: types.GET_PROJECT, payload: data }
    },
    getProjectsSuccess: (data) => {
        return { type: types.GET_PROJECT_SUCCESS, payload: data }
    },
    getProjectsError: (err) => {
        return { type: types.GET_PROJECT_ERROR, payload: err }
    },

    getOneProject: (project_id) => {
        let data = getRequest("/project/getOne", {project_id});
        return { type: types.GET_ONE_PROJECT, payload: data }
    },
    getOneProjectSuccess: (data) => {
        return { type: types.GET_ONE_PROJECT_SUCCESS, payload: data }
    },
    getOneProjectError: (err) => {
        return { type: types.GET_ONE_PROJECT_ERROR, payload: err }
    },

    updateProject: (props) => {
        var data = postRequest("/project/create", {...props});
        return { type: types.UPDATE_PROJECT, payload: data }
    },
    updateProjectSuccess: (data) => {
        return { type: types.UPDATE_PROJECT_SUCCESS, payload: data }
    },
    updateProjectError: (err) => {
        return { type: types.UPDATE_PROJECT_ERROR, payload: err }
    },
}
