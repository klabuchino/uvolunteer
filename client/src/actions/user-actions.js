import {getRequest, postRequest} from "../helpers.js";
import {types} from "../redux-types.js";


export const userActions = {

    register: (user) => {
      let data = postRequest("/users/registration", {...user});
      return { type: types.REGISTER, payload: data }
    },
    registerSuccess: (data) => {
      return { type: types.REGISTER_SUCCESS, payload: data }
    },
    registerError: (err) => {
      return { type: types.REGISTER_ERROR, payload: err }
    },
}
