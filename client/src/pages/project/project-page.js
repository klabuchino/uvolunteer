import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { actionCreators } from '../../actions/all-actions';
import {Preloader, Row, Col} from 'react-materialize';

// create a component
class ProjectPage extends Component {
    componentDidMount() {
        this.props.getOneProject(this.props.match.params.id_project);
    }

    render() {
        var {project} = this.props;

        if (project.loading || !project.current.title) {
            return (
                <Row style={{display: "flex", justifyContent: "center", alignItems: "center", height: "100vh"}}>
                  <Col>
                    <Preloader size='big'/>
                  </Col>
                </Row>
            );
        } else {
            console.log(project)
            project = project.current;
            var tags = project.tags.map((tag, ind) => {
                return (
                    <span key={ind}>{tag.title}</span>
                )
            });
            return (
                <div className="my_container">
                    <div className="crumbs"><Link to="/projects">Перейти к списку идей и проектов</Link></div>
                    <div className="proj_card_page">
                        <div className="proj_card_page__header">
                            <span className="title">{project.title}</span>
                            <div className="tags">
                                {tags}
                            </div>
                            <div className="actions">
                                <div className="likes">{project.like.length}</div>
                                <div className="other">
                                    <i className="material-icons">share</i>
                                    <i className="material-icons">bookmark_border</i>
                                </div>
                            </div>
                            <div className="maintainer">{project.organizer_id.fullName}</div>
                        </div>
                        <div className="proj_card_page__content">
                            <div className="main">
                                <div className="main__img"></div>
                                <p className="title">Подробное описание</p>
                                <p className="main__description">{project.description}</p>
                            </div>
                            <div className="detailed">
                                <p className="title">Необходимые русурсы</p>
                                <div className="detailed__resources">
                                    <div className="detailed__resources__progress_bar">
                                        <div style={{width: project.quantityMoney/project.maxQuantityMoney}} className="fill"></div>
                                    </div>
                                    <p>{project.quantityMoney/project.maxQuantityMoney}%</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            );
        }
        
    }
}

const mapStateToProps = (state) => ({
    user: state.user,
    project: state.project
});

const mapDispatchToProps = (dispatch) => {
    return {
        getOneProject: (id) => {
            return dispatch(actionCreators.getOneProject(id)).payload.then((res) => {
                !res.error ? dispatch(actionCreators.getOneProjectSuccess(res.data)) :
                dispatch(actionCreators.getOneProjectError(res.error));
            });
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProjectPage);