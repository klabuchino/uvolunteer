import React, { Component } from 'react';
import M from 'materialize-css';
import $ from 'jquery';
import { connect } from 'react-redux';
import { actionCreators } from '../../actions/all-actions';
import ProjectCard from '../../components/ProjectCard';

// create a component
class ProjectListPage extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    componentDidMount() {
        M.Tabs.init($('#tabs-swipe-demo'), {swipeable: true});
        this.props.getProjects();
    }

    _renderIdeaCards() {
        return this.props.project.list.map((pr, ind) => {
            return (
                <div className="col l4">
                    <ProjectCard key={ind} history={this.props.history} {...pr} />
                </div>
                
            )
        })
    }

    render() {
        console.log(this.props)
        return (
            <div className="my_container">
                <ul id="tabs-swipe-demo" className="tabs">
                    <li className="tab col s3"><a className="active" href="#test-swipe-1">Идеи</a></li>
                    <li className="tab col s3"><a href="#test-swipe-2">Проекты</a></li>
                </ul>
                <div id="test-swipe-1" className="row">
                    {this._renderIdeaCards.bind(this)()}
                </div>
                <div id="test-swipe-2" className="">К сожалению ни одна идея еще не собрала необходимое окличество ресурсов</div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    user: state.user,
    project: state.project
});

const mapDispatchToProps = (dispatch) => {
    return {
        getProjects: () => {
            return dispatch(actionCreators.getProjects()).payload.then((res) => {
                !res.error ? dispatch(actionCreators.getProjectsSuccess(res.data)) :
                dispatch(actionCreators.getProjectsError(res.error));
            });
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProjectListPage);
