function postRequest(url, data) {
    console.log('post url: ', url, data);

    return fetch(new Request(url, {
        method: "POST",
        headers: new Headers({
            "Content-Type": "application/x-www-form-urlencoded",
            'Accept': 'application/json',
        }),
        body: new URLSearchParams(data).toString()
    })).then((res) => {
        console.log('err::: ', res.status);
        if (res.status === 200) return res.json();
    });
}

function getRequest(url, params) {
    let url_fetch = /*config.urlServer + */url + "?";
    Object.keys(params).forEach(key => url_fetch += `${key}=${params[key]}&`);

    return fetch(url_fetch, {
        method: "GET",
    }).then((res) => {
        if (res.status === 200) return res.json();
    });
}

//For autocomplete

function normalizeForAutocomplete(arr) {
    let res = {};
    arr.forEach((item) => {
        res[item.title] = null;
    });
    return res;
}

function getAuthenticItem(arr, autocompleteItem) {
    return arr.find((item) => { return item.title === autocompleteItem });
}


// val - текущее значение поля
function isValidItem(val, authenticArr) {
    return getAuthenticItem(authenticArr, val) ? true: false;
}

//End for autocomplete


module.exports = {
  postRequest, getRequest, normalizeForAutocomplete, getAuthenticItem,
  isValidItem
};
