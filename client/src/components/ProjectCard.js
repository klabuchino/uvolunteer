import React, { Component } from 'react';
import M from 'materialize-css';

// create a component
export default class ProjectCard extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
        this.goToProjectPage = this.goToProjectPage.bind(this);
    }

    goToProjectPage() {
        this.props.history.push(`/projects/${this.props._id}`);
    }

    render() {
        var tags = this.props.tags.map((tag, ind) => {
            return (
                <span key={ind}>{tag.title}</span>
            )
        });
        var likes = this.props.likes;
        return (
            <div className="project_card">
                <div onClick={this.goToProjectPage} className="project_card__bck">
                    <div className="tags">
                        {tags}
                    </div>
                    
                </div>
                <div className="avatar"></div>
                <div className="project_card__content">
                    <p onClick={this.goToProjectPage} className="title">{this.props.title}</p>
                    <div className="project_card__progress_bar">
                        <div className="fill"></div>
                    </div>
                    <p onClick={this.goToProjectPage} className="description">{this.props.description}</p>
                    <div className="footer">
                        <span>+{likes ? likes.length : 0}</span>
                        <span>
                            <i className="material-icons">share</i>
                            <i className="material-icons">bookmark_border</i>
                        </span>
                    </div>
                </div>
            </div>
            
        );
    }
}
