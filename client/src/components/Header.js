import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { actionCreators } from '../actions/all-actions';
import SignInModal from '../modals/sign-in-modal';
import SignUpModal from '../modals/sign-up-modal';

class Header extends Component {
    logout() {
        this.props.logout(this.props.history);
    }

    openSignInModal() {
        this.signInModal.showModal();
    }

    openSignUpModal() {
        this.signUpModal.showModal();
    }


    render() {
        var links = [
            {title: "Публикации", to: "/", isActive: false},
            {title: "Идеи и проекты", to: "/projects", isActive: false},
            {title: "Пользователи", to: "/people", isActive: false},
        ].map((item, ind) => {
            let className = window.location.pathname === item.to ? "links__item active" : "links__item";
            return (
                <Link to={item.to} className={className} key={ind}>{item.title}</Link>
            )
        })
        var self_controls;
        links = (
            <div className="links">
                {links}
            </div>
        )

        self_controls = (
            <div className="self_controls">
                <div>
                    <span onClick={this.openSignInModal.bind(this)}>Войти</span> / <span onClick={this.openSignUpModal.bind(this)}>Зарегистрироваться</span>
                </div>
                <div><i className="material-icons">notifications</i></div>
            </div>
        )
        console.log(this.props.user.me.role)
        switch (this.props.user.me.role) {
          case "volunteer":
                self_controls = (
                    <div className="self_controls">
                        <div>
                            <span onClick={() => {this.props.logout(this.props.history)}}>Выйти</span>
                        </div>
                        <div><i className="material-icons">notifications</i></div>
                    </div>
                );
                break;

          case "organizer":
                self_controls = (
                    <div className="self_controls">
                        <div>
                            <span onClick={() => {this.props.logout(this.props.history)}}>Выйти</span>
                        </div>
                        <div><i className="material-icons">notifications</i></div>
                    </div>
                )
                break;

          case "admin":
                self_controls = (
                    <div>admin</div>
                )
                break;
          default:
                self_controls = (
                    <div className="self_controls">
                        <div>
                            <span onClick={this.openSignInModal.bind(this)}>Войти</span> / <span onClick={this.openSignUpModal.bind(this)}>Зарегистрироваться</span>
                        </div>
                    </div>
                )
                break;
        }

        return (
            <header>
                <SignInModal ref={(node) => {this.signInModal = node}} auth={this.props.auth} />
                <SignUpModal ref={(node) => {this.signUpModal = node}} register={this.props.register} />
                <div className="logo"></div>
                {links}
                {self_controls}
                <div className="search">
                    <input type="text"/>
                    <i className="material-icons">search</i>
                </div>
            </header>
        )

    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        logout: (history) => {
            return dispatch(actionCreators.logout()).payload.then((res) => {
                if (!res.error) {
                    history.replace("/");
                } else {
                    console.log(res.error);
                }
            });
        }
    }
}

export default connect(null, mapDispatchToProps)(Header);
