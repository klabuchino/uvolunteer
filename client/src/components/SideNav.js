import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {Collapsible, CollapsibleItem} from 'react-materialize';
import M from 'materialize-css';


class DynamicSideNav extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
        this._renderItems = this._renderItems.bind(this);
    }

    componentDidMount() {
        M.Collapsible.init(".collapsible");
    }

    _renderItems () {
        var items = this.props.data || [];

        return items.map((item, ind) => {
            let content;
            if (item.data.length > 0) {
                content = (
                    <Collapsible>
                        {
                            (item.data.map((sub_item, sub_ind) => {
                                return (
                                    <CollapsibleItem
                                        header={sub_item.title}
                                        className="dynamic_side_nav__sub_item"
                                        icon={<i className="material-icons">{sub_item.icon}</i>}
                                        key={sub_ind}
                                        onClick={sub_item.onClick ? (e) => {sub_item.onClick({}, e)} : () => {}}
                                    ></CollapsibleItem>
                                )
                            }))
                        }
                    </Collapsible>
                )
                
            }
            return (
                <CollapsibleItem 
                    header={item.title}
                    className="dynamic_side_nav__item"
                    icon={<i className="material-icons">{item.icon}</i>}
                    key={ind}>
                    {content}
                </CollapsibleItem>
            )
        });
    }

    render() {
        return (
            <div className="dynamic_side_nav">
                <div className="dynamic_side_nav__avatar"></div>
                <Collapsible accordion>
                    {this._renderItems()}
                </Collapsible>
            </div>
        );

    }
}


export default DynamicSideNav;
