var express = require('express');
var router = express.Router();
var passport = require('passport');
var BaseUser = require('../models/users/BaseUser.js').BaseUser;
var Volunteer = require('../models/users/Volunteer.js').Volunteer;
var Organizer = require('../models/users/Organizer.js').Organizer;
var async = require('async');
var debug = require('debug');
var _ = require("lodash");
var multer = require('multer');
var path = require('path');
var router = express.Router();
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './avatars')
  },
  filename: function (req, file, callback) {
    callback(null, Date.now() + path.extname(file.originalname))
  }
})
var uploading = multer({
  limits: {fileSize: 1000000},
  storage: storage
})

router.post('/registration', uploading.single('avatar'), function(req, res, next) {
    BaseUser.findOne({ email: req.body.email }, (err, user) => {
        if (!user) {
          var initUser = {
              ...req.body
          }
          if(req.file) initUser.avatar = './avatars/' + req.file.filename;
          switch (req.body.role) {
              case 'volunteer':
                  Volunteer.createNew(initUser.email, initUser.password, initUser,(err, usr) => {
                    debug('create new user: ', usr);
                    return res.json({mess: "ok"});
                  });
                  break;
              case 'organizer':
                  Organizer.createNew(initUser.email, initUser.password, initUser,(err, usr) => {
                    console.log(err);
                    debug('create new user: ', usr);
                    return res.json({mess: "ok"});
                  });
                  break;
              default:
                  return res.json({err: "not_role"});

          }
        } else res.json({error: "user_exists"})
    });
});

router.get('/init', async function(req, res, next) {
    if(req.user === undefined) {
        return res.json({role: 'null'})
    }
    var user = await BaseUser.findOne({_id: req.user._id});
    res.json({...user.toObject()})
})

router.post('/login', function(req, res, next) {
    console.log('body:', req.body);
    console.log('query:', req.query);

    passport.authenticate('local',
        function (err, user, info) {
            console.log('user:',user);
            return err
                ? res.json({ error: "failed error" })
                : user
                    ? req.logIn(user, function (err) {
                        return err
                            ? next(err)
                            : res.json({
                                  data: {
                                      user: user
                                  }
                              });
                    })
                    : res.json({error: "failed not user"});
        }
    )(req, res, next);
});

router.post('/logout', function(req, res, next) {
    req.logout();
    res.json({data: "success logout"});
});

module.exports = router;
