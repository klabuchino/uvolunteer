var express = require('express');
var router = express.Router();
var Tag = require('../models/Tag.js');

router.get('/init', function(req, res, next) {
    Tag.create();
    res.json({mess: 'ok'});
});

router.get('/tags', async function(req, res, next) {
    var tags = await Tag.get();
    res.json({tags: tags});
})
module.exports = router;
