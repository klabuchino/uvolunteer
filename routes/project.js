var express = require('express');
var router = express.Router();
var Project = require('../models/organizer/Project.js');
var ThingDonation = require('../models/donations/ThingDonation.js');
var multer = require('multer');
var path = require('path');
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './projects')
  },
  filename: function (req, file, callback) {
    callback(null, Date.now() + path.extname(file.originalname))
  }
})
var uploading = multer({
  limits: {fileSize: 1000000},
  storage: storage
})

router.post('/create', uploading.single('project'), async function(req, res, next) {
    if(Object.keys(req.body).length > 0) {
        var resourcesArray = JSON.parse(req.body.resourcesArray);
        var volunteersItemsArray = JSON.parse(req.body.volunteersItemsArray);
        var tags = JSON.parse(req.body.tags);
        var organizer_id = req.user._id;
        var pathToImage = '';
        if(req.file) pathToImage = './projects' + req.file.filename;
        var projectObj = {
            ...req.body,
            resourcesArray: resourcesArray,
            image: pathToImage,
            tags: tags,
            organizer_id: organizer_id,
            volunteersItemsArray: volunteersItemsArray
        }
        delete projectObj.resources;
        var project = await Project.create(projectObj);
        var projects = await Project.getAll();
        res.json({data: projects});
    } else res.json({error:'not_params'})
})
router.get('/getAll', async function(req, res, next) {
    let projects = await Project.getAll();
    return res.json({data: projects});
})
router.get('/getByOrganizer', async function(req, res, next) {
    if(req.query.organizer_id) {
        let projects = await Project.getByOrganizer(req.query.organizer_id);
        res.json({data: projects})
    }
    else res.json({error: 'not_organizer_id'})
})

router.get('/getOne', async function(req, res, next) {
    if(req.query.project_id) {
        let project = await Project.getOne(req.query.project_id);
        res.json({data: project});
    }
    else res.json({error: 'not_project_id'})
})
//isLike - boolean
router.post('/like', async function(req, res, next) {
    if(Object.keys(req.body).length > 0) {
        /*liked = {
        quantityLikes - number
        isLiked - boolean
        isDisliked - boolean
      }*/
        var liked = await Project.changeRating(req.body.isLike, req.user._id, req.body.project_id)
        res.json({data:liked})
    }
})

router.post('/createThingDonation', async function(req, res, next) {
    if(Object.keys(req.body).length > 0) {
        /*resources: [{
            resource: {
                type: Schema.Types.ObjectId,
                ref: 'Resource'
            },
            quantity: {
              type: Number,
              default: 0
            }
        }]*/
        let resources = JSON.parse(req.body.resources)
        var thingDonationObj = {
            ...req.body,
            resources: resources,
            sender: req.user_id
        }
        let thingDonation = await ThingDonation.create(thingDonationObj);
        res.json(thingDonation);
    }
})

router.post('/confirmThingDonation', async function(req, res, next) {
    if(req.body.thingDonation_id) {
        await ThingDonation.confirm(req.body.thingDonation_id);
        res.json({mess: 'ok'});
    }
    else res.json({error: 'not_thingDonation_id'})
})

router.get('/getNotConfim', async function(req, res, next) {
    let thingDonations = await ThingDonation.getNotConfim(req.user_id);
    res.json({data: thingDonations});
})
module.exports = router;
