
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;
var BaseUser = require('./BaseUser.js').BaseUser;

exports.Organizer =  BaseUser.discriminator('Organizer', new Schema ({
    role: {
      type: String,
      default: 'organizer'
    },
    passportSeries: {
        type: String
    },
    passportNummber: {
        type: String
    },
    avatar: {
      type: String
    }
}, {discriminatorKey: 'kind'}));
