var mongoose = require('mongoose'),
  Schema = mongoose.Schema;
var BaseUser = require('./BaseUser.js').BaseUser;

exports.Volunteer =  BaseUser.discriminator('Volunteer', new Schema ({
    role: {
      type: String,
      default: 'volunteer'
    },
    avatar: {
      type: String
    }
}, {discriminatorKey: 'kind'}));
