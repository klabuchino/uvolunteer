var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var schema = new Schema ({
    title: {
      type: String
    }
})

schema.statics.create = function() {
    var Self = this;
    const initTags = [
      {
        title: 'экология'
      },
      {
        title:'урбанизм'
      },
      {
        title:'стрит-арт'
      }
    ]

    initTags.forEach((tag) => {
        let newTag = new Self(tag);
        newTag.save();
    })
}
schema.statics.get = async function() {
    var Self = this;
    var tags = await Self.find({});
    return tags;
}
module.exports = Tag = mongoose.model('Tag', schema);
