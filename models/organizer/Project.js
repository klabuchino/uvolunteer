var mongoose = require('mongoose'),
  Schema = mongoose.Schema;
var Tag = require('../Tag.js');

var schema = new Schema ({
    title: {
      type: String
    },
    organizer_id: {
        type: Schema.Types.ObjectId,
        ref: "Organizer"
    },
    resources: [{
      type: Schema.Types.ObjectId,
      ref: "Resource"
    }],
    volunteerItems: [{
      type: Schema.Types.ObjectId,
      ref: "VolunteerItem"
    }],
    description: {
        type: String
    },
    image: {
      type: String
    },
    tags: {
        type: Array,
        default: []
    },
    quantityMoney: {
        type: Number,
        default: 0
    },
    maxQuantityMoney: {
        type: Number,
        default: 0
    },
    like: {
        type: Array,
        default: []
    },
    dislike: {
        type: Array,
        default: []
    },
    isFinished: {
        type: Boolean,
        default: false
    }
  })

schema.statics.create = async function(projectObj) {
    var Self = this;
    var Resource = require('./Resource.js');
    var VolunteerItem = require('./VolunteerItem.js');

    project = new Self(projectObj);
    project = await project.save();
    await Resource.create(projectObj.resourcesArray, project._id.toString());
    await VolunteerItem.create(projectObj.volunteersItemsArray, project._id);
    project = await Self.find({_id: project._id});
    return project;
}
schema.statics.changeRating = async function(isLike, user_id, project_id) {
    var Self = this;
    let isLiked = false;
    let isDisliked = false;
    var project = await Self.find({_id: project_id});
    if(isLike) {
        if(!project.like.includes(user_id)){
            like.push(user_id);
            isLiked = true;
        }
        let indexDislike = project.dislike.indexOf(user_id);
        if(indexDislike !== -1) {
            project.dislike.splice(indexDislike, 1);
        }
    }
    else {
        if(!project.dislike.includes(user_id)){
            like.push(user_id);
            isDisliked = true;
        }
        let indexlike = project.like.indexOf(user_id);
        if(indexlike !== -1) {
            project.like.splice(indexDislike, 1);
        }
    }
    project = await project.save();
    let quantityLikes = project.like.length - project.dislike.length;
    return {
      quantityLikes: quantityLikes,
      isLiked: isLiked,
      isDisliked: isDisliked
    }
}

schema.statics.getAll = async function() {
    var Self = this;
    var Resource = require('./Resource.js');
    var VolunteerItem = require('./VolunteerItem.js');
    let projects = await Self.find().populate({path:'resources', model: Resource}).populate({path:'volunteerItems', model: VolunteerItem});
    return projects;
}

schema.statics.getByOrganizer = async function(organizer_id) {
    var Self = this;
    var Resource = require('./Resource.js');
    var VolunteerItem = require('./VolunteerItem.js');
    let projects = await Self.find({organizer_id:organizer_id}).populate({path:'resources', model: Resource}).populate({path:'volunteerItems', model: VolunteerItem});
    return projects;
}

schema.statics.getOne = async function(project_id) {
  var Self = this;
  var Resource = require('./Resource.js');
  var VolunteerItem = require('./VolunteerItem.js');
  var Organizer = require('../users/Organizer.js').Organizer;
  let project = await Self.findOne({_id: project_id}).populate({path:'resources', model: Resource}).populate({path:'volunteerItems', model: VolunteerItem}).populate({path:'organizer_id', model: Organizer});
  return project;
}
module.exports = Project = mongoose.model('Project', schema);
