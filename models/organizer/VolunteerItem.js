var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var schema = new Schema ({
    title: {
        type: String
    },
    description: {
        type: String
    },
    project: {
      type: Schema.Types.ObjectId,
      ref: 'Project'
    },
    maxQuantity: {
        type: Number,
        default: 0
    },
    quantity: {
        type: Number
    },
    volunteers: {
      type: Array,
      default: []
    }
})

schema.statics.create = async function(volunteersItemsArray, project_id) {
  var Self = this;
  var Project = require('./Project.js');
  console.log(volunteersItemsArray);
  let arrayVolunteerItem_id = [];
  if(volunteersItemsArray) {
      for(var i = 0; i < volunteersItemsArray.length; ++i) {
          delete volunteersItemsArray[i]._id;
          volunteersItemsArray[i].project = project_id;
          let volunteerItem = new Self(volunteersItemsArray[i]);
          volunteerItem = await volunteerItem.save();
          arrayVolunteerItem_id.push(volunteerItem._id);
      }
      let project = await Project.update({_id: project_id}, {volunteerItems: arrayVolunteerItem_id});
  }
}

module.exports = VolunteerItem = mongoose.model('VolunteerItem', schema);
