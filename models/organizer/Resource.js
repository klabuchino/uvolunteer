var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var schema = new Schema ({
  title: {
    type: String
  },
  project: {
    type: Schema.Types.ObjectId,
    ref: 'Project'
  },
  cost: {
      type: Number,
      default: 0
  },
  quantity: {
    type: Number,
    default: 0
  },
  maxQuantity: {
      type: Number,
      default: 0
  },
  isFinished: {
    type: Boolean,
    default: false
  }
})

schema.statics.create = async function(addResourcesArray, project_id) {
    var Self = this;
    var Project = require('./Project.js');
    let money = 0;
    let arrayResources_id = [];
    if(addResourcesArray) {
        for(var i = 0; i < addResourcesArray.length; ++i) {
            addResourcesArray[i].project = project_id;
            delete addResourcesArray[i]._id;
            console.log(addResourcesArray[i]);
            let resource = new Self(addResourcesArray[i]);
            resource = await resource.save();
            arrayResources_id.push(resource._id);
            money = money + resource.cost;
        }
        let project = await Project.update({_id:project_id}, {resources: arrayResources_id, maxQuantityMoney:money});
      }
}
  module.exports = Resource = mongoose.model('Resource', schema);
