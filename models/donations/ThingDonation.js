var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var schema = new Schema ({
    project: {
        type: Schema.Types.ObjectId,
        ref: 'Project'
    },
    sender: {
        type: Schema.Types.ObjectId,
        ref: 'BaseUser'
    },
    //'получателя присылать не надо'
    recipient: {
        type: Schema.Types.ObjectId,
        ref: 'Organizer'
    },
    resources: [{
        resource: {
            type: Schema.Types.ObjectId,
            ref: 'Resource'
        },
        quantity: {
          type: Number,
          default: 0
        }
    }],
    isConfirm: {
        type: Boolean,
        default: false
    }
})
schema.statics.create = async function(thingDonationObj) {
    var Self = this;
    var Project = require('../organizer/Project.js');
    var BaseUser = require('../users/BaseUser.js');
    var Organizer = require('../users/Organizer');

    let sender = await BaseUser.findOne({_id: thingDonationObj.sender});
    let project = await Project.findOne({_id: thingDonationObj.project});
    let recipient = project.organizer_id;
    thingDonationObj.recipient = recipient._id;

    if(sender) {
        let newThingDonation = new Self(thingDonationObj);
        newThingDonation = await newThingDonation.save();
        return {data: newThingDonation}
    }
    else return {error: 'not_user'};
}

var countMoney = function(qty, maxQty, donatQty, cost) {
    let costOne = cost / maxQty;
    let tail = qty - maxQty;
    let money = costOne * (donatQty - tail);
    return money;
}

schema.statics.confirm = async function(thingDonation_id) {
    var Self = this;
    var Resource = require('../organizer/Resource.js');
    var Project = require('../organizer/Project.js')

    let thingDonation = await Self.findOne({_id: thingDonation_id});

    let money = 0;
    let resources = thingDonation.resources;
    for(let i = 0; i < resources.length; ++i) {
        let resource = await Resource.findOne({_id: resources[i].resource});
        resource.quantity += resources[i].quantity;
        if(resource.quantity >= resource.maxQuantity) {
            money += countMoney(resource.quantity, resource.maxQuantity, resources[i].quantity, resource.cost);
            resource.quantity = resource.maxQuantity;
            resource.isFinished = true;
        }
        else {
            let costOne = resource.cost / resource.maxQuantity;
            money += (costOne * resources[i].quantity);
        }
        resource = await resource.save();
    }
    let project = await Project.findOne({_id: thingDonation.project});
    project.quantityMoney += money;
    if(project.quantityMoney >= project.maxQuantity) {
        project.quantityMoney = project.maxQuantity;
        project.isFinished = true;
    }
    project = await project.save();
    await Self.update({_id: thingDonation_id}, {isConfirm: true});
}

schema.statics.getNotConfirm = async function(sender_id) {
    var Self = this;
    let thingDonations = await Self.find({sender: sender_id, isConfirm: false});
    return thingDonations;
}
module.exports = ThingDonation = mongoose.model('ThingDonation', schema);
